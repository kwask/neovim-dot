set nocompatible

syntax on
filetype on
filetype plugin on
filetype indent on

" plugins
source ~/.config/nvim/plugins.vim

" config settings
source ~/.config/nvim/config.vim

" themeing
source ~/.config/nvim/theme.vim

" key mappings
source ~/.config/nvim/keys.vim

