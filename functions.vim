function DeleteToLineEnd()
    :normal d$
endfunction

function YankToLineEnd()
    :normal y$
endfunction

function SelectAll()
    :normal ggVG
endfunction

function Reindent()
    :normal gg=G<C-o><C-o>
endfunction

