set number "display line numbers
set relativenumber "display relative line numbers
set wrap
set expandtab
set tabstop=4
set shiftround
set shiftwidth=4
set ttyfast

" text wrapping
set tw=79
set fo+=t
set fo-=l

" search settings
set hlsearch
set incsearch
set ignorecase
set smartcase

